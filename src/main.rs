use axum::Router;
use tokio;
use tower_http::services::ServeDir;

#[tokio::main]
async fn main() {
    let router = Router::new().nest_service("/", ServeDir::new("static"));
    let tcp_listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    axum::serve(tcp_listener, router).await.unwrap();
}
